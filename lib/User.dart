class User {
  final int id;
  final String name;
  final String userName;
  final String email;
  final String password;
  final String role;
  final bool online;

  User(
      {this.id,
      this.name,
      this.userName,
      this.email,
      this.password,
      this.role,
      this.online});
}
