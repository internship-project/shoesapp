class Shoes {
  final int id;
  final String brand;
  final String shoeType;
  final String shoeSize;

  Shoes({this.id, this.brand, this.shoeType, this.shoeSize});
}
